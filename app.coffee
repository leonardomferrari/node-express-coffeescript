express = require 'express'
http = require 'http'
mongoose = require 'mongoose'
app = express()

PORT = 3000

# La aplicación está preparada para manejar
# proveedores MongoDB disponibles en Heroku: MongoHQ y MongoLab
app.set 'storage-uri',
    process.env.MONGOHQ_URL or
    process.env.MONGOLAB_URI or
    'mongodb://localhost/widgets'

app.configure ->
  app.set 'port', process.env.PORT or PORT
  app.set 'views', "#{__dirname}/views"
  app.set 'view engine', 'jade'
  app.use express.favicon()
  app.use express.logger('dev') if app.get('env') is 'development'
  
  #Incluimos el middleware bodyParser que analiza solicitud 
  #de cuerpo para los parámetros pasados ​​en que se disponga
  #como req.body 
  app.use express.bodyParser()
  
  app.use express.methodOverride()
  #app.use express.cookieParser 'your secret here'
  #app.use express.session()
  app.use app.router
  app.use require('connect-assets')(src: "#{__dirname}/assets")
  app.use express.static "#{__dirname}/public"
  require('./middleware/404')(app)

app.configure 'development', ->
  app.use express.errorHandler()
  app.locals.pretty = true


autoload = require('./config/autoload')(app)
autoload "#{__dirname}/helpers", true
autoload "#{__dirname}/assets/js/shared", true
autoload "#{__dirname}/models"
autoload "#{__dirname}/controllers"
autoload "#{__dirname}/collections"

###
Mongo Connection
###

mongoose.connect app.get('storage-uri'), { db: { safe: true }}, (err) ->
  console.log "Mongoose - connection error: " + err if err?
  console.log "Mongoose - connection OK"


## ROUTES
# require('./config/routes')(app)
app.get '/', (req, res) ->
	res.render 'index',
		locals:
			title: 'Hello World :)!'
			

http.createServer(app).listen app.get('port'), ->
  port = app.get 'port'
  env = app.settings.env
  console.log "Escuchando en el port #{port} en modo #{env}"

module.exports = app
