# node-express-coffeescript

A simple Node template with Express, CoffeeScript, Jade and Connect.


## Dependencies
Esta template se puede utilizar para crear aplicaciones [Node](http://nodejs.org) utilizando;

* [express](https://github.com/visionmedia/express)
* [CoffeeScript](http://jashkenas.github.com/coffee-script)
* [Jade](http://jade-lang.com/)
* [underscore](http://documentcloud.github.com/underscore/)
* [underscore.string](https://github.com/epeli/underscore.string)
* [connect-assets](https://github.com/adunkman/connect-assets)
* [mongoose](http://mongoosejs.com/)
* [Bootstrap v2.3.1](http://bootstrapdocs.com/v2.3.1/docs/javascript.html)

## Install

```
# Install Node and npm on Ubuntu 12.04

sudo apt-get update
sudo apt-get install python-software-properties python g++ make
sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install nodejs

git clone git@bitbucket.org:leonardomferrari/node-express-coffeescript.git

cd [project-name]


# Install Dependencies
# Se instalara todo lo que esta definido en el archivo package.json

sudo npm install

# Ejecutar la aplicacion
coffee app.coffee or node app.js

# Otra forma de ejecutar la aplicacion: npm start

Para mayor comodidad, podemos utilizar npm para lanzar el servidor mediante la adición un scripts de comandos en package.json.

{
  ...
  "scripts": {
    "start": "coffee -w app.coffee"
  }
}
A partir de ahora, podemos iniciar el servidor con npm start. Adicionando el parametro -w logramos que la aplicación se ejecuta en un modo de observer, es decir, se vuelve a cargar cada vez que surgen en el código fuente.

```

## Fuentes:

* [Tom Wilson](https://github.com/twilson63) for express-coffee the original behind this template and readme 
* [Jeremy Ashkenas](https://github.com/jashkenas) for coffee-script
* [TJ Holowaychuk](https://github.com/visionmedia) for express, jade and connect
* [Ryan Dahl](https://github.com/ry) for node

https://github.com/alfrednerstu/node-express-coffeescript